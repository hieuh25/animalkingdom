import requests


def get_todo(id):
    r = requests.get(f'https://jsonplaceholder.typicode.com/todos/{id}')
    r.raise_for_status()
    return r.json()
