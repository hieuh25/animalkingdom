import random
import logging

from .api import get_todo

logger = logging.getLogger(__name__)


class Animal():
    def __init__(self, *, name):
        self.name = name

    def speak(self):
        print('Hey i dont speak')

    def yo(self):
        return 'Yo man wazzup in the hood?'


class Dog(Animal):
    def bark(self):
        print('Gau')

    def speak(self):
        self.bark()


class Cat(Animal):
    def meow(self):
        print('Meow')

    def speak(self):
        self.meow()

    def get_random_todo(self):
        logger.warning('I drink and I know things')

        random_todo = get_todo(random.randint(1, 300))
        return random_todo['title']
